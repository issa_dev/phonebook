**postman send and save data to database example:**

Method: POST

URL: http://localhost:8088/api/v1/contacts

Body: Json

```
{
 "name":"issa",
 "phoneNumber": "09124956239",
 "email":"issa.khodadadi88@gmail.com",
 "organization":"Asre-Danesh",
 "github":"issa-dev",
 "repositories":[
     {
        "repositoriesName": "phone-book",
        "contact":{
        "contactId":1
        }
    }
 ]
}
```


**postman retrive data example:**

Method: GET

URL: http://localhost:8088/api/v1/contacts/search

Params:    

each param can be null*


|     key      | value  |
| ------------ | ------ |
| name         | String |
| phoneNumber  | String |
| email        | String |
| organization | String |
| github       | String |




**access to H2 console:**

after running the project, copy the following address into your browser:

http://localhost:8088/api/v1/h2-console

and put these data: 

JDBC URL:  jdbc:h2:mem:testdb

Username: issadb

Password: 1234
