package com.example.phonebook.controller;

import com.example.phonebook.entity.Contact;
import com.example.phonebook.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

import static com.example.phonebook.specification.ContactSpecification.*;

/**
 * @author IssA Khodadadi, 1/1/2021, 11:40 PM
 */
@RepositoryRestController
@RequestMapping("contacts")
public class ContactController {
    @Autowired
    private ContactRepository contactRepository;

    @PostMapping()
    public ResponseEntity<Contact> registerNewContact(@RequestBody Contact contact) {
        return ResponseEntity.ok(contactRepository.save(contact));
    }

    @GetMapping("search")
    public ResponseEntity<List<Contact>> searchContacts(@RequestParam(required = false) String name,
                                                        @RequestParam(required = false) String phoneNumber,
                                                        @RequestParam(required = false) String email,
                                                        @RequestParam(required = false) String organization,
                                                        @RequestParam(required = false) String github) {
        return ResponseEntity.ok(contactRepository.findAll(Specification.where(withName(name))
                                                            .and(withPhoneNumber(phoneNumber))
                                                            .and(withEmail(email))
                                                            .and(withOrganization(organization))
                                                            .and(withGithub(github))));
    }
}
