package com.example.phonebook.entity;

import javax.persistence.*;
import java.util.*;

/**
 * @author IssA Khodadadi, 1/1/2021, 11:27 PM
 */
@Entity
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long contactId;
    private String name;
    private String phoneNumber;
    private String email;
    private String organization;
    private String github;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy="contact")
    private List<Repositories> repositories;

    public List<Repositories> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<Repositories> repositories) {
        this.repositories = repositories;
    }


    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long id) {
        this.contactId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getGithub() {
        return github;
    }

    public void setGithub(String github) {
        this.github = github;
    }
}
