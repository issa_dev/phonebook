package com.example.phonebook.entity;

import javax.persistence.*;

/**
 * @author IssA Khodadadi, 1/5/2021, 12:32 AM
 */
@Entity
public class Repositories {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long repositoriesId;
    private String repositoriesName;

    @ManyToOne()
    @JoinColumn(name = "contact_id")
    private Contact contact;

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Long getRepositoriesId() {
        return repositoriesId;
    }

    public void setRepositoriesId(Long id) {
        this.repositoriesId = id;
    }

    public String getRepositoriesName() {
        return repositoriesName;
    }

    public void setRepositoriesName(String repositoryName) {
        this.repositoriesName = repositoryName;
    }

}
