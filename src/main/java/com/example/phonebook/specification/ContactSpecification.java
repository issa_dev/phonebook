package com.example.phonebook.specification;

import com.example.phonebook.entity.Contact;
import org.springframework.data.jpa.domain.Specification;

/**
 * @author IssA Khodadadi, 1/3/2021, 2:27 PM
 */
public class ContactSpecification {
    public static Specification<Contact> withName(String name) {
        if (name == null) {
            return null;
        } else {
            return ((root, query, cb) -> cb.equal(root.get("name"), name));
        }
    }

    public static Specification<Contact> withPhoneNumber(String phoneNumber) {
        if (phoneNumber == null) {
            return null;
        } else {
            return ((root, query, cb) -> cb.equal(root.get("phoneNumber"), phoneNumber));
        }
    }

    public static Specification<Contact> withEmail(String email) {
        if (email == null) {
            return null;
        } else {
            return ((root, query, cb) -> cb.equal(root.get("email"), email));
        }
    }

    public static Specification<Contact> withOrganization(String organization) {
        if (organization == null) {
            return null;
        } else {
            return ((root, query, cb) -> cb.equal(root.get("organization"), organization));
        }
    }

    public static Specification<Contact> withGithub(String github) {
        if (github == null) {
            return null;
        } else {
            return ((root, query, cb) -> cb.equal(root.get("github"), github));
        }
    }

    public static Specification<Contact> withRepositories(String repositories) {
        if (repositories == null) {
            return null;
        } else {
            return ((root, query, cb) -> cb.equal(root.get("github"), repositories));
        }
    }
}
